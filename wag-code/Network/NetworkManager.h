//
//  NetworkManager.h
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StackOverflowUserModel.h"
#import "UserModel.h"

@interface NetworkManager : NSObject

typedef void (^UserCompletion) (NSArray<UserModel*>* _Nullable  userModels, NSError* _Nullable  error);

+ (void)getUsersWithUserModel:(StackOverflowUserModel *)stackOverflowUserModel completion:(UserCompletion)completion;

@end
