//
//  NetworkManager.m
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking/AFNetworking.h>

@interface NetworkManager()

+ (AFHTTPSessionManager *)manager;

@end

@implementation NetworkManager

+ (void)getUsersWithUserModel:(StackOverflowUserModel *)stackOverflowUserModel completion:(UserCompletion)completion {
    NSString* url = [stackOverflowUserModel URLString];
    NSDictionary* parameters = [stackOverflowUserModel parameters];
    
    [[self manager] GET:url
             parameters:parameters
               progress:NULL
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    [self parseUserResponseObject:responseObject completion:completion];
                }
                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"Error: %@", error);
                    completion(NULL, error);
                }
     ];
}

+ (void)parseUserResponseObject:(id _Nullable)responseObject completion:(UserCompletion)completion {
    NSMutableArray<UserModel*>* userObjects = [NSMutableArray array];
    
    NSDictionary* dictResponse = (NSDictionary *)responseObject;
    NSArray* userArray = [dictResponse objectForKey:@"items"];
    
    for (NSDictionary* userDict in userArray) {
        UserModel* userModel = [[UserModel alloc] initWithDict:userDict];
        [userObjects addObject:userModel];
    }
    
    completion(userObjects, NULL);
}

#pragma mark - Initilization

+ (AFHTTPSessionManager *)manager {
    return [AFHTTPSessionManager manager];
}

@end
