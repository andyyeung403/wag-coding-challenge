//
//  LoadingImageView.m
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "LoadingImageView.h"
#import "UIImageView+AFNetworking.h"

@interface LoadingImageView()

@property (nonatomic, strong) UIActivityIndicatorView* loadingView;
@property (nonatomic, nullable) NSString* url;

@end

@implementation LoadingImageView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setUpLoadingView];
}

- (void)setUpLoadingView {
    [self addSubview:self.loadingView];
    
    [[self.loadingView.centerXAnchor constraintEqualToAnchor:self.centerXAnchor] setActive:YES];
    [[self.loadingView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor] setActive:YES];
}

- (void)clear {
    [self.loadingView stopAnimating];
    
    self.url = NULL;
    self.image = NULL;
}

- (void)loadImage:(NSString *)url {
    [self.loadingView startAnimating];
    self.url = url;
    
    NSURL* nsurl = [NSURL URLWithString:url];
    NSURLRequest* urlRequest = [NSURLRequest requestWithURL:nsurl
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
    __weak typeof(self) weakSelf = self;
    
    [self setImageWithURLRequest:urlRequest
                placeholderImage:NULL
                         success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                             [weakSelf.loadingView stopAnimating];
                             [weakSelf setImage:image request:request];
                         } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                             [weakSelf.loadingView stopAnimating];
                             NSLog(@"Error downloading image: %@", error);
                         }];
}

- (void)setImage:(UIImage *)image request:(NSURLRequest *)request {
    // Make sure the downloaded image is the correct image for this view.
    if ([request.URL.relativeString isEqualToString:self.url]) {
        self.image = image;
    }
}

#pragma mark - Initilization

- (UIActivityIndicatorView *)loadingView {
    if (!_loadingView) {
        _loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_loadingView setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    
    return _loadingView;
}

@end
