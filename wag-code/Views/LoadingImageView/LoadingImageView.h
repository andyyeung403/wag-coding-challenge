//
//  LoadingImageView.h
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingImageView : UIImageView

- (void)clear;
- (void)loadImage:(NSString *)url;

@end
