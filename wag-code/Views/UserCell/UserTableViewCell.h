//
//  UserTableViewCell.h
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface UserTableViewCell : UITableViewCell

- (void)setUpWithUserModel:(UserModel*)userModel;

@end
