//
//  UserTableViewCell.m
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "UserTableViewCell.h"
#import "LoadingImageView.h"

@interface UserTableViewCell()

@property (weak, nonatomic) IBOutlet LoadingImageView *gravatarView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *bronzeLabel;
@property (weak, nonatomic) IBOutlet UILabel *silverLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;

@end

@implementation UserTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
     
    [self.gravatarView clear];
    
    self.userName.text = NULL;
    self.bronzeLabel.text = NULL;
    self.silverLabel.text = NULL;
    self.goldLabel.text = NULL;
}

- (void)setUpWithUserModel:(UserModel *)userModel {
    [self.gravatarView loadImage:userModel.profileImage];
    
    self.userName.text = userModel.displayName;
    self.bronzeLabel.text = userModel.bronzeCount.stringValue;
    self.silverLabel.text = userModel.silverCount.stringValue;
    self.goldLabel.text = userModel.goldCount.stringValue;
}

@end
