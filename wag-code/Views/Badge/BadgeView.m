//
//  BadgeView.m
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "BadgeView.h"

@implementation BadgeView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self commonInit];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
            
    [self commonInit];
    
    return self;
}

- (void)commonInit {
    self.layer.cornerRadius = self.frame.size.height / 2;
}

- (void)setBadgeColor:(UIColor *)badgeColor {
    _badgeColor = badgeColor;
    
    self.backgroundColor = _badgeColor;
}

@end
