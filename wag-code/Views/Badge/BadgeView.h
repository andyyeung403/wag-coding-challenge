//
//  BadgeView.h
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Needs to make sure the BadgeView has the same height and width, because the badge is a circle, not a triangle.
 */
@interface BadgeView : UIView

@property (nonatomic, strong) IBInspectable UIColor* badgeColor;

@end
