//
//  StackOverflowUserModel.h
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Model class to deal with StackoverFlow User API.
 Only retrieves the first page of user data (default 30 users).
 */
@interface StackOverflowUserModel : NSObject

- (NSString *)URLString;
- (NSDictionary *)parameters;

@end
