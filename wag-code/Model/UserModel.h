//
//  UserModel.h
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, strong, readonly) NSNumber* userId;
@property (nonatomic, strong, readonly) NSString* displayName;
@property (nonatomic, strong, readonly) NSString* profileImage;
@property (nonatomic, strong, readonly) NSNumber* bronzeCount;
@property (nonatomic, strong, readonly) NSNumber* silverCount;
@property (nonatomic, strong, readonly) NSNumber* goldCount;

- (instancetype)initWithDict:(NSDictionary*)dict;

@end
