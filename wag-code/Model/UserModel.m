//
//  UserModel.m
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    
    if (self) {
        _userId = [dict objectForKey:@"user_id"];
        _displayName = [dict objectForKey:@"display_name"];
        _profileImage = [dict objectForKey:@"profile_image"];
        
        NSDictionary* badgeDict = [dict objectForKey:@"badge_counts"];
        _bronzeCount = [badgeDict objectForKey:@"bronze"];
        _silverCount = [badgeDict objectForKey:@"silver"];
        _goldCount = [badgeDict objectForKey:@"gold"];
    }
    
    return self;
}

@end
