//
//  StackOverflowUserModel.m
//  wag-code
//
//  Created by Andy Yeung on 6/4/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "StackOverflowUserModel.h"

@interface StackOverflowUserModel()

@property (nonatomic, strong) NSString* method;
@property (nonatomic, strong) NSString* site;
@property (nonatomic, strong) NSNumber* pageSize;
@property (nonatomic, strong) NSNumber* page;

@end


@implementation StackOverflowUserModel

- (NSString *)URLString {
    return [NSString stringWithFormat:@"https://api.stackexchange.com/2.2/%@", self.method];
}

- (NSDictionary *)parameters {
    return @{@"page": self.page,
             @"pagesize": self.pageSize,
             @"site": self.site};
}

#pragma mark - Initilization

- (NSString *)method {
    if (!_method) {
        _method = @"users";
    }
    
    return _method;
}

- (NSString *)site {
    if (!_site) {
        _site = @"stackoverflow";
    }
    
    return _site;
}

/**
 Returns 30 users per page (default).

 @return The number of users per page.
 */
- (NSNumber *)pageSize {
    if (!_pageSize) {
        _pageSize = @30;
    }
    
    return _pageSize;
}

- (NSNumber *)page {
    if (!_page) {
        _page = @1;
    }
    
    return _page;
}

@end
