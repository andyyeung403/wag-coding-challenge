//
//  ViewController.m
//  wag-code
//
//  Created by Andy Yeung on 6/1/18.
//  Copyright © 2018 Andy Yeung. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "StackOverflowUserModel.h"
#import "UserTableViewCell.h"

@interface ViewController() <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView* tableView;

@property (strong, nonatomic) StackOverflowUserModel* stackOverflowUserModel;
@property (strong, nonatomic) NSMutableArray<UserModel*>* userObjects;

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTableView];
    [self loadUsers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Clear data with memory warning
    self.userObjects = [NSMutableArray array];
    [self.tableView reloadData];
}

- (void)setTableView {
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    UINib* userCellNib = [UINib nibWithNibName:@"UserTableViewCell" bundle:NULL];
    [self.tableView registerNib:userCellNib forCellReuseIdentifier:@"userCell"];
}

- (void)loadUsers {
    __weak typeof(self) weakSelf = self;
    
    [NetworkManager getUsersWithUserModel:self.stackOverflowUserModel completion:^(NSArray<UserModel *> * _Nullable userModels, NSError * _Nullable error) {
        if (userModels) {
            weakSelf.userObjects = [userModels mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
            });
        }
    }];
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UserTableViewCell* cell = (UserTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"userCell"];
    
    [cell setUpWithUserModel:self.userObjects[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.userObjects.count;
}

#pragma mark - Initilization

- (StackOverflowUserModel *)stackOverflowUserModel {
    if (!_stackOverflowUserModel) {
        _stackOverflowUserModel = [[StackOverflowUserModel alloc] init];
    }
    
    return _stackOverflowUserModel;
}

- (NSMutableArray *)userObjects {
    if (!_userObjects) {
        _userObjects = [NSMutableArray array];
    }
    
    return _userObjects;
}

@end
